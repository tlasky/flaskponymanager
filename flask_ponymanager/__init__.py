from .former import (
    EntityForm,
    EntityFormer
)
from .view import (
    ManagerView,
    EntityView
)
from .ponymanager import PonyManager

__version__ = '3.1.8'
__author__ = 'David Tláskal'
