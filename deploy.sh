rm -r ./dist/*
rm -r ./build/lib/flask_ponymanager/*
cp -r ./flask_ponymanager/static ./build/lib/flask_ponymanager
cp -r ./flask_ponymanager/templates ./build/lib/flask_ponymanager
python3 setup.py install
git add .
git commit -m "Refactoring etc..."
git push
python3 -m twine upload ./dist/*
