from setuptools import (
    setup,
    find_packages
)
from flask_ponymanager import (
    __version__,
    __author__
)

install_requires = [
    'Flask',
    'Pony',
    'WTForms',
    'libsass'
]

open('requirements.txt', 'w').write('\n'.join(install_requires))

setup(
    name='Flask-PonyManager',
    version=__version__,
    url='https://tlasky@bitbucket.org/tlasky/flaskponymanager',
    license='BSD',
    author=__author__,
    author_email='da.tlaskal@gmail.com',
    description='Similar to Flask-Admin but for Pony ORM.',
    long_description=open('README.md', 'r').read(),
    long_description_content_type='text/markdown',
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=install_requires,
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
