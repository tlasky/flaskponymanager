import os
import sys

base_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(base_path, '..'))

from flask import (
    Flask,
    redirect,
    url_for
)
from pony.orm import (
    Database,
    Required,
    Optional,
    Set,
    db_session
)
from pony.flask import Pony
from pony.orm.core import TransactionIntegrityError
from werkzeug.security import (
    generate_password_hash,
    check_password_hash
)
from flask_ponymanager import (
    PonyManager,
    ManagerView,
    EntityView
)

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'VerySecretKey123456789*'
db = Database()
db.bind(provider='sqlite', filename='./database.db', create_db=True)
pony = Pony(app)

"""
Creating manager
"""
manager = PonyManager(app, db)


class Role(db.Entity):
    name = Required(str, unique=True)
    description = Optional(str)
    users = Set('User')

    def __str__(self) -> str:
        """
        Role will be listed as it's name in manager forms.
        """
        return self.name


class User(db.Entity):
    email = Required(str, unique=True)
    password_hash = Required(str)
    roles = Set('Role')

    def __init__(self, *args, **kwargs):
        """
        Check if password is has and if not, then generate hash from password.
        """
        password_hash = kwargs.get('password_hash')
        if password_hash and not password_hash.startswith('pbkdf2:sha256:'):
            kwargs['password_hash'] = self.generate_hash(password_hash)
        super().__init__(*args, **kwargs)

    def __str__(self) -> str:
        """
        User will be listed as it's email in manager forms.
        """
        return self.email

    @staticmethod
    def generate_hash(value: str) -> str:
        return generate_password_hash(value)

    def set_password(self, value: str) -> None:
        self.password_hash = self.generate_hash(value)

    def check_password(self, value: str) -> bool:
        return check_password_hash(self.password_hash, value)


db.generate_mapping(create_tables=True)

with db_session:
    try:
        admin_role = Role(
            name='admin',
            description='Administrator.'
        )

        editor_role = Role(
            name='editor',
            description='Editor.'
        )

        User(
            email='da.tlaskal@gmail.com',
            password_hash=User.generate_hash('123456'),
            roles=[admin_role, editor_role]
        )

        db.commit()
    except TransactionIntegrityError:
        pass

"""
Adding manager views
"""
manager_view = ManagerView(manager)
manager.add_view(manager_view)
manager.add_view(EntityView(manager, User))
manager.add_view(EntityView(manager, Role))
manager.register()


@app.route('/')
def index():
    return redirect(url_for(manager_view.endpoint))


if __name__ == '__main__':
    app.run('0.0.0.0', 5000)
